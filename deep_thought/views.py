# Create your views here.
from django.template import RequestContext
from django.http import Http404
from deep_thought.models import Question
from django.shortcuts import render_to_response
from deep_thought.forms import QuestionForm

def Index(request):
    if request.method == "POST":
        q_form = QuestionForm(request.POST)
        if q_form.is_valid():
            q_form.save()
    else:
        q_form = QuestionForm
    try:
        q = Question.objects.all()
    except Question.DoesNotExist:
        raise Http404
    return render_to_response('home.html', {'questions': q,"q_form": q_form}, RequestContext(request))