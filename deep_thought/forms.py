from django import forms
from django.forms import ModelForm
from deep_thought.models import *
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout
from crispy_forms.bootstrap import FormActions

LOW_URGENCY = 1
MEDIUM_URGENCY = 2
HARD_URGENCY = 3

URGENCY = (
    (LOW_URGENCY,"Low"),
    (MEDIUM_URGENCY,"Urgent"),
    (HARD_URGENCY,"ASAP!")
)
class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ("title","text","tags","urgency")

    tags = forms.CharField(
        required=False,
        help_text = "ex: mail, django forms, rock'n roll, disney "
    )

    urgency = forms.ChoiceField(
        choices=URGENCY
    )

    helper= FormHelper()
    helper.form_class= 'form-horizontal'
    helper.form_tag= True
    helper.form_method = "POST"
    helper.layout = Layout("title","text","tags","urgency",
        FormActions(
            Submit('save_changes', 'Save changes'),
            Submit('cancel', 'Cancel', css_class="btn-cancel"),
        )
    )



class AnswerForm(ModelForm):
    class Meta:
        model = Answer

class UserConfigForm(ModelForm):
    class Meta:
        model = UserConfig
