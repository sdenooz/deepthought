from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from tinymce.models import HTMLField

# Create your models here.

class Tag(models.Model):
    title = models.CharField(_("Tag"),max_length=255)

class Question(models.Model):
    title = models.CharField(_("Title"),max_length=255)
    text = HTMLField(_("Question"))
    urgency = models.IntegerField(_("Urgency"))
    tags = models.ManyToManyField(Tag)
    user = models.ForeignKey(User, related_name='question_user')
    followers = models.ManyToManyField(User, related_name='question_followers')

class Answer(models.Model):
    text = models.TextField(_("Answer"))
#    votes = models.ManyToManyField(_("Votes"), related_name='answer_votes')
    answer_the_question = models.BooleanField(_("Answer the question"))
    user = models.ForeignKey(User)
    question = models.ForeignKey(Question)

class Vote(models.Model):
    is_good = models.BooleanField()
    is_bad = models.BooleanField()
    answer = models.ForeignKey(Answer, related_name='vote_answer')
    user = models.ForeignKey(User, related_name='vote_user')

class UserConfig(models.Model):
    user = models.ForeignKey(User)
    mails_periodicity = models.IntegerField(_("Mails periodicity"))
    followed_tag = models.ManyToManyField(Tag)